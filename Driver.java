/**
 * The driver class for this coding experiment.
 * 
 * @author Daniel Cisneros
 * @version 1.0
 * 
 */

 public class Driver {
     public static void main(String[] args) {
         Secret mySecret = new Secret("This is not a code assignment, this is a Tide Commercial.");
         System.out.println(mySecret);

         mySecret.encrypt();
         System.out.println(mySecret);

         mySecret.decrypt();
         System.out.println(mySecret);
     }
 }