/**
 * A simple interface example
 * 
 * @author Daniel Cisneros 
 * @version 1.0
 * 
 */

 public interface Encryptable {
     public void encrypt();
     public String decrypt();
 }