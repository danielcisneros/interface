
/**
 * A secret message that we can encrypt and decrypt.
 * 
 * @author Daniel Cisneros'
 * @version 1.0
 * 
 */

import java.util.Random;

public class Secret implements Encryptable {
    private String message;
    private boolean encrypted;
    private int shift;
    private Random generator;

    public Secret(String inputMessage) {
        message = inputMessage;
        encrypted = false;
        generator = new Random();
        shift = generator.nextInt(10) + 5;
    }

    /**
     * This encryptes your message - friend.
     */
    public void encrypt() {
        if (!encrypted) {
            String masked = "";
            for (int count = 0; count < message.length(); count++) {
                masked = masked + (char) (message.charAt(count) + shift);
            }
            message = masked;
            encrypted = true;
        }
    }

    public String decrypt() {
        if (encrypted) {
            String unmasked = "";
            for (int count = 0; count < message.length(); count++) {
                unmasked = unmasked + (char) (message.charAt(count) - shift);
            }
            encrypted = false;
            message = unmasked;
        }
        return message;
    }

    public boolean isEncrypted() {
        return encrypted;
    }

    public String toString() {
        return message;
    }

}